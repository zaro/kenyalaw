# Downloader for http://kenyalaw.org/

This is a simple scraper for windows that allows you to download automatically documents from http://kenyalaw.org/


# How to use

1. Download a release from [here](https://gitlab.com/zaro/kenyalaw/-/wikis/home)

1. Extract the kenya-law-downloader-\*.zip anywhere

2. Run the kenya-law-downloader.exe in the newly extracted folder

3. Select Date From/To and Output folder where the downloaded files will be stored

4. If needed select Court, and maximum files to be downloaded. If these fields are left empty it means all courts and no limit on how many files will be downloaded.

5. Click start

6. Wait for the download to complete

7. Files are in the selected output folder

8. If there are any problems with Windows versions prior to 10 , like errors that some .dll files cannot be found, please install the appropriate update from the following URL:
https://support.microsoft.com/en-us/help/2999226/update-for-universal-c-runtime-in-windows


# Author

Svetlozar Argirov <zarrro [AT] gmail [DOT] com>