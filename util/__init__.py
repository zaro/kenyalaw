import os


def traverse_module_dir(name, mdir):
    result = []
    for f in os.listdir(mdir):
        if f == '__init__.py':
            result.append(name)
        elif f.endswith('.py'):
            if not f.startswith('__'):
                mname = f.replace('.py', '')
                result.append(name + '.' + mname)
        elif os.path.isdir(os.path.join(mdir, f)):
            result += traverse_module_dir(name + '.' + f, os.path.join(mdir, f))
    return result
