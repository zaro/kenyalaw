# -*- mode: python -*-
import os, types
import scrapy
import urllib

scrapy_dir = os.path.dirname(scrapy.__file__)
urllib_dir = os.path.dirname(urllib.__file__)

def traverse_module_dir(name, mdir):
    result = []
    for f in os.listdir(mdir):
        if f == '__init__.py':
            result.append(name)
        elif f.endswith('.py'):
            if not f.startswith('__'):
                mname = f.replace('.py', '')
                result.append(name + '.' + mname)
        elif os.path.isdir(os.path.join(mdir, f)):
            result += traverse_module_dir(name + '.' + f, os.path.join(mdir, f))
    return result


hiddenimports = (
    traverse_module_dir('kenyalaw', 'kenyalaw') +
    traverse_module_dir('scrapy', scrapy_dir) +
    traverse_module_dir('urllib', urllib_dir)
)

block_cipher = None

datas = [
                (os.path.join(scrapy_dir,'VERSION'),'scrapy'),
                (os.path.join(scrapy_dir,'mime.types'),'scrapy'),
                ('scrapy.cfg','.'),
            ]

print(datas)
a = Analysis(['downloader.py', 'spider.py', 'gui.py'],
             pathex=['C:\\Users\\Me\\git\\kenyalaw'],
             binaries=[],
             datas=datas,
             hiddenimports=hiddenimports,
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          exclude_binaries=True,
          name='kenya-law-downloader',
          debug=False,
          strip=False,
          upx=True,
          console=False,
          icon='resources\\gui.ico' )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               name='kenya-law-downloader')
