from guizero import App, Box, Text, TextBox,  PushButton, Combo
from tkinter.filedialog import askdirectory
from tkinter import messagebox, Toplevel, Frame, Label
import subprocess
from appdirs import user_config_dir
from ttkcalendar import CalendarDialog
import os
import json
import glob
import time
import calendar
from datetime import datetime


class Gui:
    def __init__(self, app_exe):
        self.spider = None
        self.spider_retcode = None
        self.app_exe = app_exe

    def load_state(self):
        cfg_file = os.path.join(user_config_dir(), 'state.json')
        if os.path.exists(cfg_file):
            try:
                with open(cfg_file) as fp:
                    return json.load(fp)
            except Exception:
                pass
        return {}

    def save_state(self):
        cfg_file = os.path.join(user_config_dir(), 'state.json')
        try:
            with open(cfg_file, 'w') as fp:
                return json.dump(self.state(), fp)
        except Exception as e:
            print('Error saving:', e)
            pass
        return {}

    def state(self):
        state = {}
        out_dir = self.out_dir.value.strip()
        if out_dir:
            state['output_dir'] = out_dir

        date_from = self.date_from.value.strip()
        if date_from:
            state['date_from'] = date_from

        date_to = self.date_to.value.strip()
        if date_to:
            state['date_to'] = date_to

        court = self.court.value.strip()
        if court:
            state['court'] = court

        try:
            max_files = int(self.max_files.value)
            state['max_files'] = max_files
        except Exception:
            pass

        return state

    def set_state(self, state):
        out_dir = state.get('output_dir', None)
        if out_dir:
            out_dir = self.out_dir.value = out_dir

        date_from = state.get('date_from', None)
        if date_from:
            self.date_from.value = date_from

        date_to = state.get('date_to', None)
        if date_to:
            date_to = self.date_to.value = date_to

        court = state.get('court', None)
        if court:
            court = self.court.value = court


        max_files = state.get('max_files', None)
        if max_files:
            self.max_files.value = max_files


    def build_command_line(self):
        args = self.app_exe[:]
        args.append('run_spider')

        state = self.state()

        if 'output_dir' not in state:
            messagebox.showerror('Invalid directory', 'You must select Output directory!')
            return
        args.append(state['output_dir'])

        date_from = state.get('date_from', None)
        if date_from:
            args.append(f'KL_DATE_FROM={date_from}')
        else:
            messagebox.showerror('Invalid Date', 'You must select From date!')
            return

        date_to = state.get('date_to', None)
        if date_to:
            args.append(f'KL_DATE_TO={date_to}')
        else:
            messagebox.showerror('Invalid Date', 'You must select To Date!')
            return

        court = state.get('court', None)
        if court:
            court = COURTS[court]
            args.append(f'KL_COURT={court}')

        max_files = state.get('max_files', None)
        if max_files:
            args.append(f'KL_MAX_FILES={max_files}')

        return (args, state['output_dir'])

    def start_spider(self):
        cmdline, output_dir = self.build_command_line()
        print('Starting:', cmdline)
        if not cmdline:
            return
        self.save_state()
        self.spider = subprocess.Popen(cmdline)
        self.dl_button.text = "Cancel"
        self.run_label.text = 'Running...'
        self.running_in_output_dir = output_dir
        self.running_start_time = time.time()
        self.running_stats_file = None

    def stop_spider(self):
        if self.spider:
            self.spider.terminate()
        self.dl_button.text = "Start"
        self.run_label.text = ''

    def check_spider(self):
        if self.spider:
            self.spider_retcode = self.spider.poll()
            if self.spider_retcode is not None:
                self.spider = None
                self.stop_spider()
                return False
            return True
        return False

    def is_running(self):
        return self.check_spider()

    def start_download(self):
        if self.spider:
            self.stop_spider()
        else:
            self.start_spider()

    def select_dir(self):
        self.dirname = askdirectory()
        if self.dirname:
            self.out_dir.value = self.dirname

    def read_stats(self):
        if not self.running_stats_file:
            statsGlob = os.path.join(self.running_in_output_dir, 'stats.*.json')
            flist = sorted(glob.glob(statsGlob), reverse=True)
            if flist:
                statsFile = flist[0]
                sfCtime = os.stat(statsFile).st_ctime
                if sfCtime > self.running_start_time:
                    self.running_stats_file = statsFile
        if self.running_stats_file:
            try:
                with open(self.running_stats_file) as fp:
                    return json.load(fp)
            except Exception:
                pass

    def timer(self):
        if self.check_spider():
            stats = self.read_stats()
            if stats and 'file_count' in stats:
                file_count = stats['file_count']
                self.run_label.text = f'{file_count} files downloaded'
                # print(stats)
        self.app.after(1000, self.timer)

    def on_closing(self):
        if self.is_running():
            if messagebox.askokcancel("Downloader running", "Kill current download process?"):
                self.stop_spider()
                self.app.destroy()
        else:
            self.app.destroy()

    def set_date_from(self):
        df = self.date_from.value
        try:
            d = datetime.strptime(df, "%Y-%m-%d")
        except Exception:
            d = datetime.now()
        cd = CalendarDialog(self.app.tk, title='Select date From', year=d.year, month=d.month)
        if cd.result:
            self.date_from.value = cd.result.date().isoformat()

    def set_date_to(self):
        dt = self.date_to.value
        try:
            d = datetime.strptime(dt, "%Y-%m-%d")
        except Exception:
            d = datetime.now()
        cd = CalendarDialog(self.app.tk, title='Select date To', year=d.year, month=d.month)
        if cd.result:
            self.date_to.value = cd.result.date().isoformat()

    def start_gui(self):
        font = "Helvetica"
        font_size = 18
        self.app = App(title="Kenyalaw.org crawler", width=800, height=480)
        row = 0
        label = Text(self.app, text="kenyalaw.org Downloader", size=24, color="black")
        d_box = Box(self.app, layout="grid", align="top", width="fill")
        #d_box.bg ="red"

        df_label = Text(d_box, text="From Date", size=18, color="black", grid=[0, row], align="left")
        df_box = Box(d_box, grid=[1, row], width="fill")
        #df_box.bg ="green"
        self.date_from = TextBox(df_box, align="left", width="fill")
        self.date_from.font=(font, font_size)
        df_select = PushButton(df_box, text="Select", align="right", command=self.set_date_from)

        row += 1
        dt_label = Text(d_box, text="To Date", size=18, color="black", grid=[0, row], align="left")
        dt_box = Box(d_box, grid=[1, row], width="fill")
        self.date_to = TextBox(dt_box, align="left", width="fill")
        self.date_to.font=(font, font_size)
        dt_select = PushButton(dt_box, text="Select", align="left", command=self.set_date_to)

        row += 1
        self.date_from.font=(font, font_size)
        dt_label = Text(d_box, text="Court", size=18, color="black", grid=[0, row], align="left")
        max_len = len(sorted(list(COURTS.keys()), key=lambda x: len(x), reverse=True)[0])
        self.court = Combo(d_box, options=[' '*max_len] + list(COURTS.keys()), grid=[1, row], align="left", width="fill")

        row += 1
        od_label = Text(d_box, text="Output folder", size=18, color="black", grid=[0, row], align="left")
        od_box = Box(d_box, grid=[1, row], width="fill")
        self.out_dir = TextBox(od_box, align="left", width='fill')
        self.out_dir.font=(font, font_size)
        out_dir_select = PushButton(od_box, text="Browse",  align="left", command=self.select_dir)

        row += 1
        mf_label = Text(d_box, text="Max results to download", size=18, color="black", grid=[0, row])
        self.max_files = TextBox(d_box, grid=[1, row], align='left')
        self.max_files.font=(font, font_size)
        # #### DEMO
        # self.max_files.set('10')
        # self.max_files.config(state='disabled')
        # ###
        self.dl_button = PushButton(self.app, text="Start", command=self.start_download)
        self.dl_button.font=(font, font_size)
        self.run_label = Text(self.app, text="", width="fill", height="fill")
        self.run_label.font=(font, font_size*2)

        self.set_state(self.load_state())
        self.app.after(1000, self.timer)
        self.app.when_closed =  self.on_closing
        self.app.display()


COURTS = {
  'All Courts of Appeal': '189999',
  'All High Courts': '190000',
  'All Employment and Labour Relations Courts': '190001',
  'Environmental And Land Court': '190002',
  'Supreme Court of Kenya': '23',
  'Court of Appeal at Busia': '57',
  'Court of Appeal at Eldoret': '22',
  'Court of Appeal at Kisii': '56',
  'Court of Appeal at Kisumu': '10',
  'Court of Appeal at Malindi': '11',
  'Court of Appeal at Mombasa': '47',
  'Court of Appeal at Nairobi': '9',
  'Court of Appeal at Nakuru': '21',
  'Court of Appeal at Nyeri': '20',
  'High Court at Bomet': '71',
  'High Court at Bungoma': '15',
  'High Court at Busia': '14',
  'High Court at Chuka': '80',
  'High Court at Eldoret': '13',
  'High Court at Embu': '8',
  'High Court at Garissa': '29',
  'High Court at Homabay': '38',
  'High Court at Kabarnet': '87',
  'High Court at Kajiado': '82',
  'High Court at Kakamega': '16',
  'High Court at Kapenguria': '78',
  'High Court at Kericho': '4',
  'High Court at Kerugoya': '39',
  'High Court at Kiambu': '86',
  'High Court at Kisii': '24',
  'High Court at Kisumu': '3',
  'High Court at Kitale': '17',
  'High Court at Kitui': '58',
  'High Court at Kwale': '85',
  'High Court at Lodwar': '40',
  'High Court at Machakos': '5',
  'High Court at Makueni': '88',
  'High Court at Malindi': '7',
  'High Court at Marsabit': '81',
  'High Court at Meru': '6',
  'High Court at Migori': '63',
  'High Court at Mombasa': '1',
  "High Court at Murang'a": '41',
  'High Court at Nairobi (Milimani Commercial Courts Commercial and Tax Division)': '19',
  'High Court at Nairobi (Milimani Law Courts)': '18',
  'High Court at Naivasha': '66',
  'High Court at Nakuru': '2',
  'High Court at Nanyuki': '83',
  'High Court at Narok': '65',
  'High Court at Nyahururu': '89',
  'High Court at Nyamira': '79',
  'High Court at Nyeri': '12',
  'High Court at Siaya': '77',
  'High Court at Thika': '133',
  'High Court at Voi': '62',
  'Employment and Labour Relations at Eldoret': '134',
  'Employment and Labour Relations Court at Kericho': '69',
  'Employment and Labour Relations Court at Kisumu': '34',
  'Employment and Labour Relations Court at Mombasa': '33',
  'Employment and Labour Relations Court at Nairobi': '27',
  'Employment and Labour Relations Court at Nakuru': '45',
  'Employment and Labour Relations Court at Nyeri': '35',
  'East African Court of Justice': '26',
  'Interim Independent Constitutional Dispute Resolution Court': '28',
  'Court of Appeal for East Africa': '30',
  'Judges and Magistrates Vetting Board': '31',
  'National Environment Tribunal - Nairobi': '25',
  'Commissions and AdHoc Tribunals': '32',
  'Dispute Resolution Committee (IEBC)': '50',
  "Political Party's Tribunal": '51',
  'Election Petition in Magistrate Courts': '52',
  'Environment and Land Court at Bungoma': '68',
  'Environment and Land Court at Busia': '75',
  'Environment and Land Court at Chuka': '122',
  'Environment and Land Court at Eldoret': '46',
  'Environment and Land Court at Embu': '123',
  'Environment and Land Court at Garissa': '124',
  'Environment and Land Court at Kajiado': '125',
  'Environment and Land Court at Kakamega': '126',
  'Environment and Land Court at Kericho': '67',
  'Environment and Land Court at Kerugoya': '70',
  'Environment and Land Court at Kisii': '55',
  'Environment and Land Court at Kisumu': '60',
  'Environment and Land Court at Kitale': '61',
  'Environment and Land Court at Kwale': '132',
  'Environment and Land Court at Machakos': '84',
  'Environment and Land Court at Makueni': '127',
  'Environment and Land Court at Malindi': '48',
  'Environment and Land Court at Meru': '76',
  'Environment and Land Court at Migori': '128',
  'Environment and Land Court at Mombasa': '53',
  'Environment and Land Court at Muranga': '129',
  'Environment and Land Court at Nairobi': '54',
  'Environment and Land Court at Nakuru': '59',
  'Environment and Land Court at Narok': '130',
  'Environment and Land Court at Nyahururu': '135',
  'Environment and Land Court at Nyandarua': '131',
  'Environment and Land Court at Nyeri': '49',
  'Environment and Land Court at Thika': '90',
  'Kadhis Court at Bungoma': '104',
  'Kadhis Court at Eldoret': '97',
  'Kadhis Court at Garissa': '103',
  'Kadhis Court at Garissa (Dadaab)': '111',
  'Kadhis Court at Garsen': '118',
  'Kadhis Court at Hola': '99',
  'Kadhis Court at Isiolo': '93',
  'Kadhis Court at Kajiado': '113',
  'Kadhis Court at Kakamega': '117',
  'Kadhis Court at Kilifi': '96',
  'Kadhis Court at Kisumu': '94',
  'Kadhis Court at Kitale': '120',
  'Kadhis Court at Kitui': '109',
  'Kadhis Court at Kwale': '102',
  'Kadhis Court at Lamu': '100',
  'Kadhis Court at Lodwar (Kakuma)': '119',
  'Kadhis Court at Machakos': '110',
  'Kadhis Court at Malindi': '95',
  'Kadhis Court at Mandera': '108',
  'Kadhis Court at Marsabit': '116',
  'Kadhis Court at Migori': '112',
  'Kadhis Court at Mombasa': '74',
  'Kadhis Court at Moyale': '98',
  "Kadhis Court at Murang'a": '115',
  'Kadhis Court at Nairobi (Kibera Law Courts)': '91',
  'Kadhis Court at Nairobi (Milimani Commercial Court)': '92',
  'Kadhis Court at Nakuru': '106',
  'Kadhis Court at Nyeri': '101',
  'Kadhis Court at Thika': '114',
  'Kadhis Court at Voi': '105',
  'Kadhis Court at Wajir': '121',
  'Kadhis Court at Wajir (Habaswein)': '107'
}
