FOR /f "tokens=1-8 delims=:./ " %%G IN ("%date%") DO (
SET BUILD_DATE=%%J-%%H-%%I
)
echo Build: %BUILD_DATE%
pyinstaller.exe -y downloader.spec
cd dist
7z a -tzip kenya-law-downloader-%BUILD_DATE%.zip kenya-law-downloader\
cd ..
