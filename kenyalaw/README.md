Extract Court list:

In scrapy shell:

    fetch('http://kenyalaw.org/caselaw/')
    { o.xpath('.//text()').extract_first(): o.xpath('.//@value').extract_first()  for o in response.css('select[name="court[]"]').css('option') }
