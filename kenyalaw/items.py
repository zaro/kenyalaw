# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class KenyalawItem(scrapy.Item):
    file_urls = scrapy.Field()
    file_ids = scrapy.Field()
    files = scrapy.Field()
