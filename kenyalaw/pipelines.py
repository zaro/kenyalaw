# -*- coding: utf-8 -*-

from scrapy.pipelines.files import FilesPipeline
import re
import os
import shutil
import logging

logger = logging.getLogger('KenyalawPipeline')


def fileid_from_url(url):
    m = re.search(r'(\d+)/(\w+)$', url)
    if m:
        return m[1]


class KenyalawPipeline(FilesPipeline):

    def media_downloaded(self, response, request, info, *, item=None):
        r = super().media_downloaded(response, request, info, item=item)
        cdHeader = response.headers['Content-Disposition'].decode("utf8", errors="replace")
        r['cd_header'] = cdHeader
        filename_re = None
        if re.search(r'filename="', cdHeader):
            filename_re = r'filename="([^"]+)"'
        else:
            filename_re = r'filename=(\S+)'
        m = re.search(filename_re, cdHeader)
        if m:
            r['original_filename'] = m[1].strip('"')
        settings = info.spider.settings
        fileStore, fileOutput = settings.get('FILES_STORE'), settings.get('FILES_OUTPUT_DIR')
        print(fileStore, fileOutput)
        if fileStore and fileOutput:
            fileInfo = r
            url = r['url']
            srcFile = os.path.join(fileStore, fileInfo['path'])
            filename = fileInfo.get('original_filename')
            if not filename:
                filename = os.path.basename(srcFile)
            file_id = fileid_from_url(url)
            if file_id:
                filename = f'{file_id}-{filename}'
            dstFile = os.path.join(fileOutput, filename)
            shutil.copyfile(srcFile, dstFile)
            logger.info(f'Copy {srcFile} -> {dstFile}')
            fileInfo['moved_to'] = dstFile
            if info.spider.save_stats:
                info.spider.save_stats()
        return r
