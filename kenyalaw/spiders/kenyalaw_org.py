# -*- coding: utf-8 -*-
import scrapy
import re
import sys
import os
from ..items import KenyalawItem
import json
from datetime import datetime


class DateTimeEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, datetime):
            return o.isoformat()

        return json.JSONEncoder.default(self, o)


class KenyalawOrgSpider(scrapy.Spider):
    name = 'kenyalaw_org'
    allowed_domains = ['kenyalaw.org']
    fileTypes = {}
    maxFiles = sys.maxsize

    def start_requests(self):
        if 'KL_FILE_TYPES' in self.settings:
            ftypes = self.settings['KL_FILE_TYPES'].split(',')
            for ftype in ftypes:
                self.fileTypes[ftype.lower()] = True
        if not self.fileTypes:
            self.fileTypes = {'pdf': True}

        formdata = {}
        if 'KL_DATE_FROM' in self.settings:
            formdata['date_from'] = self.settings['KL_DATE_FROM']
        if 'KL_DATE_TO' in self.settings:
            formdata['date_to'] = self.settings['KL_DATE_TO']
        if 'KL_COURT' in self.settings:
            formdata['court[]'] = self.settings['KL_COURT']
        if 'KL_MAX_FILES' in self.settings:
            try:
                self.maxFiles = int(self.settings['KL_MAX_FILES'])
            except Exception:
                pass

        # ### DEMO
        # self.maxFiles = 10
        # ###

        return [
            scrapy.FormRequest(
                "http://kenyalaw.org/caselaw/cases/advanced_search/",
                formdata=formdata,
                callback=self.parse
            )
        ]

    def parse(self, response):
        for href in response.css('a.show-more::attr("href")').extract():
            if self.maxFiles <= 0:
                print('Reached max files, quitting!')
                return
            yield scrapy.Request(href, callback=self.parse_view)
        nextPageUrl = response.css('.pagination ul.pages li.next a::attr("href")').extract_first()
        if nextPageUrl:
            yield scrapy.Request(nextPageUrl, callback=self.parse)

    def parse_view(self, response):
        if self.maxFiles <= 0:
            return
        item = KenyalawItem()
        urls = []
        features = response.css('.features')
        if features:
            exportUrls = sorted(features[0].css('a::attr("href")').extract(), reverse=True)
            fileTypes = self.fileTypes.copy()
            for url in exportUrls:
                m = re.search(r'/(\w+)$', url)
                if m and m[1].lower() in fileTypes:
                    urls.append(url)
                    del fileTypes[m[1].lower()]

            exportIds = sorted(features[0].css('a::attr("onclick")').extract())
            ids = []
            for js in exportIds:
                m = re.search(r'\("(\d+)"\);', js)
                if m :
                    ids.append(m[1])
            ids = list(set(ids))

            item['file_urls'] = urls + [
                f"http://kenyalaw.org/caselaw/caselawreport/index_original.php?id={i}" for i in ids
            ]
            item['file_ids'] = ids
            yield item
        self.maxFiles -= len(urls)

    def save_stats(self):
        statsFile = self.settings.get('STATS_FILE', None)
        if statsFile:
            with open(statsFile, 'w') as fp:
                json.dump(self.crawler.stats.get_stats(), fp, cls=DateTimeEncoder)
