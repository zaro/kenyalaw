from scrapy.crawler import Crawler, CrawlerProcess
from scrapy.utils.project import get_project_settings
from scrapy.utils.log import configure_logging
import scrapy.utils.misc
import scrapy.core.scraper

from kenyalaw.spiders.kenyalaw_org import KenyalawOrgSpider
from appdirs import user_cache_dir
from contextlib import redirect_stderr
import os
import json
import shutil
import re
from datetime import datetime
import logging


def warn_on_generator_with_return_value_stub(spider, callable):
    pass

scrapy.utils.misc.warn_on_generator_with_return_value = warn_on_generator_with_return_value_stub
scrapy.core.scraper.warn_on_generator_with_return_value = warn_on_generator_with_return_value_stub

logger = logging.getLogger('spider.py')


def mkdir(d):
    try:
        os.makedirs(d)
    except FileExistsError:
        pass


def get_file_store_dir():
    appname = "KenyalawDownloader"
    appauthor = "BrokenByDesign"
    return user_cache_dir(appname, appauthor)


def filename_from_url(url):
    m = re.search(r'(\d+)/(\w+)$', url)
    if m:
        return f'{m[1]}.{m[2]}'
    m = re.search(r'/(\w+)$', url)
    return m[1]


def real_start_spider(output_directory, suffix, *args):
    file_cache_dir = os.path.join(output_directory, '_cache')
    mkdir(file_cache_dir)
    feedUri = os.path.join(output_directory, f'items.{suffix}.jl')

    settings = get_project_settings()
    settings['FILES_STORE'] = file_cache_dir
    settings['FILES_OUTPUT_DIR'] = output_directory
    settings['FEED_URI'] = f'file://{feedUri}' if os.path.isabs(feedUri) else feedUri
    settings['KENYALAWPIPELINE_FILES_EXPIRES'] = -1
    settings['LOG_FILE'] = os.path.join(output_directory, f'crawl.{suffix}.log')
    settings['STATS_FILE'] = os.path.join(output_directory, f'stats.{suffix}.json')
    settings['LOG_STDOUT'] = True
    settings['LOG_ENABLED'] = True
    for arg in args:
        kvl = arg.split('=', 1)
        if len(kvl) > 1:
            settings[kvl[0]] = kvl[1]

    configure_logging(settings)
    logger.debug('Full project settings: %s', dict(settings))

    crawler = Crawler(KenyalawOrgSpider, settings)
    process = CrawlerProcess(settings)

    process.crawl(crawler)
    process.start()
    # save final stats
    crawler.spider.save_stats()
    # remove cache directory
    shutil.rmtree(file_cache_dir)


def start_spider(output_directory, *args):
    mkdir(output_directory)
    suffix = datetime.now().isoformat().replace(':', '-')
    with open(os.path.join(output_directory, f'error.{suffix}.log'), 'a') as f:
        with redirect_stderr(f):
            real_start_spider(output_directory, suffix, *args)
