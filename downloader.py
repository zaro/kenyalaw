#!/usr/bin/env python
import sys
import os
import argparse
from spider import start_spider
from gui import Gui


def get_executable():
    if getattr(sys, 'frozen', False):
        python_exe = None
        application_exe = sys.executable
        return [application_exe]
    elif __file__:
        python_exe = sys.executable
        application_exe = __file__
        return [python_exe, application_exe]


parser = argparse.ArgumentParser(description='Scrape kenyalaw.org')
parser.add_argument('params', metavar='PARAM=VALUE', nargs='*')
args = parser.parse_args()

if args.params and args.params[0] == 'run_spider':
    if len(args.params) > 1:
        start_spider(args.params[1], *args.params[2:])
    else:
        parser.print_usage()
else:
    Gui(get_executable()).start_gui()
